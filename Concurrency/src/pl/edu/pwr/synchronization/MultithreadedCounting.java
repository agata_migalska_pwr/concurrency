package pl.edu.pwr.synchronization;

class Counter {

	private volatile int counter = 10;

	public void printCount() throws InterruptedException {
		while (counter > 0) {
			counter = counter - 1;
			System.out
					.println(Thread.currentThread().getName() + " " + counter);
			Thread.sleep(100);
		}
	}

}

class CountingThread extends Thread {
	private Counter counter;

	CountingThread(String name, Counter counter) {
		this.counter = counter;
		this.setName(name);
	}

	public void run() {
		try {
			counter.printCount();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Wątek " + this.getName() + " kończy działanie.");
	}

}

public class MultithreadedCounting {

	public static void main(String args[]) {

		Counter counter = new Counter();

		CountingThread thread1 = new CountingThread("Jaś", counter);
		CountingThread thread2 = new CountingThread("Małgosia", counter);

		thread1.start();
		thread2.start();

		try {
			thread1.join();
			thread2.join();
		} catch (Exception e) {
			System.out.println("Interrupted");
		}
	}
}
